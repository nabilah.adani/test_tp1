from django.shortcuts import render
from .models import aNews

# Create your views here.
def news(request): 
    news_list = aNews.objects.all().values()

    x = {'news_list' : news_list}
    return render(request, 'news.html', x)