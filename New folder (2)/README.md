# tp1-ppw
[![pipeline status](https://gitlab.com/rian_audrian/tp1-ppw/badges/master/pipeline.svg)](https://gitlab.com/rian_audrian/tp1-ppw/commits/master)  [![coverage report](https://gitlab.com/rian_audrian/tp1-ppw/badges/master/coverage.svg)](https://gitlab.com/rian_audrian/tp1-ppw/commits/master)


Link app: https://ppw-tp1-kel13.herokuapp.com

## Nama Kelompok

Muhammad Audrian (1706043790)
Patricia Anugrah Setiani (1706074884)
Nabilah Adani Nurulizzah (1706979386)
