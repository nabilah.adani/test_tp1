from django.shortcuts import render
from .models import Event

# Create your views here.
def events(request):
	events_list = Event.objects.all().values()

	args = {'events_list': events_list}
	return render(request, 'events.html', args)