from django.db import models
from datetime import datetime

# Create your models here.
class Event(models.Model):
	name = models.CharField(max_length = 100)
	location = models.CharField(max_length = 100)
	description = models.CharField(max_length = 300)
	category = models.CharField(max_length = 50)
	date = models.DateField(default=datetime.now, blank=True)
	start_time = models.TimeField(default='12:00')
	end_time = models.TimeField(default='12:00')
	# entrants = models.ManyToManyField(User)

	def __str__(self):
		return self.name