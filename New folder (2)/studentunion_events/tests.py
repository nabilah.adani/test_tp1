from django.test import TestCase, Client
from django.urls import resolve
from .views import events
from .models import Event

# Create your tests here.
class StudentUnionEventsTest(TestCase):
	def test_events_page_exists(self):
		response = Client().get('/events')
		self.assertEqual(response.status_code, 200)

	def test_events_uses_correct_template(self):
		found = resolve('/events')
		self.assertEqual(found.func, events)

	def test_events_adds_event(self):
		test = Event.objects.create(
			name = "Zipzoo",
			location ="Zoozoo",
			description = "Zipzoozoozoo",
			date = "2018-10-17",
			start_time = "12:00",
			end_time = "13:00"
			)
		self.assertEqual(Event.objects.all().count(), 1)

	def test_events_can_participate(self):
		pass

	def test_events_unknown_user_name(self):
		pass

	def test_events_delete_event(self):
		Event.objects.all().delete()
		self.assertEqual(Event.objects.all().count(), 0)